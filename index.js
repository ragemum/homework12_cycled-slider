let num = 1;
let intervalId = null;

const runButton = document.getElementById('button-run-id');
const stopButton = document.getElementById('button-stop-id');
const img = document.getElementById('img-id');

runButton.addEventListener('click', function () {
    console.log (intervalId);
    if (!intervalId) {
        intervalId = setInterval(() => {
            img.setAttribute("src", `./img/${num}.jpg`);
            num = num < 4 ? num + 1 : 1;
        }, 3000);
    }
})

stopButton.addEventListener('click', function () {
    clearInterval(intervalId);
    intervalId = null;
});